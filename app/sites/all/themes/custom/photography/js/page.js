(function($){

    // assume we're NOT dealing with a big-image
    var h1Opacity = false;

    $(window).load(function(){
    
        //initialize flexslider
        $('.flexslider').flexslider();
        
        // if there's a big-image, we'll need to prepend the over state
        if($('.big-image').html() != null && $('.big-image-over').html() == null){
            // if we have a div with the class "big-image", prepend a div that will act as our gradient
            $('.big-image').prepend('<div class="big-image-over"></div>');
            // hide it right away
            $('.big-image-over').css({opacity: 0});
            // set a flag so that we know to fade in the h1
            h1Opacity = true;
            
            // set initial styles on h1 too
            $('h1').css({opacity: 0, zIndex: 2, paddingLeft: "1em", margin: "-2em 0 1em", color: "#fff"});
        }            
    });

    $(window).scroll(function(){
        
        // do our math
        // first, set body object
        var body = $('body'),
            // then get position of document while we scroll
            yPos = body.scrollTop(),
            // then get the total height of body
            total = $('body').height(),
            // then find the viewport (and shorten it up a bit - hence the dividing by 1.5)
            viewable = (total - $(window).height())/1.5,
            // then figure out the opacity of things!
            opacity = yPos/viewable;
            
        // if we're dealing with a big-image
        if(h1Opacity){
            // fade in the h1
            $('h1').css({opacity: opacity});
        }
        
        // and fade in the gradient div
        $('.big-image-over').css({opacity: opacity});
        
    });
    
    $(document).ready(function(){
        // hide all our captions on page load
        $('.single-image .caption').hide();
        
        // on hover, show caption
        // on hover out (second function), hide caption again
        $('.single-image').hover(
          function(){
            $(this).find('.caption').fadeIn(100);
          },
          function(){
            $(this).find('.caption').fadeOut(100);
          }
        
        );
    });
    

}(jQuery))